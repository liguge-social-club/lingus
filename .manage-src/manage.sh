#!/bin/bash

PROJECT__SCORES_RELDIR=src/scores
PROJECT__EXPORTS_RELDIR=exports

#-------------------- help()
help()
{
	echo "Usage : $0 [GLOBAL-OPTIONS] <COMMAND> [COMMAND-ARGS]"
	echo "Commands:"
	echo "  show <INFO>            Shows information."
	echo "  build <WHAT> [SCORE]   Builds some output file (midi, pdf, mp3...)."
	echo "  watch <WHAT> [SCORE]   Watches changes in the dependencies of the given score"
	echo "                         and builds the output files when needed."
	echo "  deps <LILYPOND_FILE>   Shows all lilypond local dependencies for the given file"
	echo "  gen-makefile [-f]      Generates a makefile"
	echo "  help [COMMAND]         Gets help for the given command."
	echo "Options:"
	echo "  -q --quiet  Be quiet"
}

#-------------------- include()
include()
{
	for include in "$@"; do
		source "$SCRIPT_ROOT_DIR/$include.sh"
	done
}



# Get basic help:
[[ $1 =~ ^(-h|--help)$ ]] && { help; exit; }



# Set important files and directories:
SCRIPT_ROOT_DIR="$( cd "$(dirname "$(realpath "${BASH_SOURCE[0]}")" )" && pwd )"
PROJECT_DIR="$(realpath "$SCRIPT_ROOT_DIR"/..)"
PROJECT__SCORES_DIR=$(realpath "$PROJECT_DIR/$PROJECT__SCORES_RELDIR")
PROJECT__EXPORTS_DIR=$(realpath "$PROJECT_DIR/$PROJECT__EXPORTS_RELDIR")
ENV_FILE="$PROJECT_DIR/env"



# Parse the env file:
[[ -f $ENV_FILE ]] && source "$ENV_FILE"



# Handle completion:
if [[ $1 == --get-completion ]]; then
	include commands completion
	shift
	do_completion "$@"
	exit
fi



# Setup default options:
verbose=1 # Not quiet



# Include additional scripts:
includes=(
	'util/ansi'
	'util/text_formatting'
	'util/cli_parse'
	'util/commands'
	'util/assertions'
	'util/misc'

	'include/scores'
	'include/export'
	'include/deps'
	'include/output'
)
include "${includes[@]}"



# Source the env file:
if [[ -f $env_file ]]; then
	set -a
	source "$env_file"
fi



# Check environment variables:
# variables_must_be_defined  VAR1  VAR2 ...



# Parse cli arguments:
command_args=()
for arg in "$@"; do
	# Define the command:
	if [[ -z $cli_command && $arg != -* ]]; then
		cli_command="$arg"
		continue
	fi

	# Command arguments:
	if [[ -n $cli_command ]]; then
		command_args+=( "$arg" )
		continue
	fi

	# Global options:
	case $arg in
		-q|--quiet)
			verbose=
			;;
		*)
			fatal "Unrecognized argument or option: ${bold}${arg}${reset}"
			;;
	esac
done



# Check and setup the command to execute:
[[ -z $cli_command ]] && fatal "Command expected (see -h for help)"

# Execute the main command:
execute_command "$cli_command" "${command_args[@]}"



# See you, space cowboy
