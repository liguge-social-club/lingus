MAKEFILE_TEMPLATE="$SCRIPT_ROOT_DIR/templates/Makefile"
MAKEFILE="$PROJECT_DIR/Makefile"

#-------------------- command_help()
command_help()
{
    echo "gen-makefile [-f]  Generates a makefile. Use -f to overwrite an existing makefile."
}

#-------------------- command_exec()
command_exec()
{
	local force=
	[[ $1 == -f ]] && force=1

	if [[ -f $MAKEFILE && -z $force ]]; then
		fatal "The file '${bold}$MAKEFILE${reset}' already exists. Use option -f to overwrite."
	fi

	gen_makefile >"$MAKEFILE"
	[[ -n $verbose ]] && echo "Ok." >&2
}

#-------------------- gen_makefile()
gen_makefile()
{
	local all_targets all_midi_pdf_targets
	all_targets=()
	all_midi_pdf_targets=()

	local progress_message="Generating the makefile..."
	[[ -n $verbose ]] && echo -n "$progress_message" >&2

	cat "$MAKEFILE_TEMPLATE"
	echo
	local score_names
	readarray -t score_names < <(get_score_names)
	local nb_scores=${#score_names[@]}
	let i=1

	local song_name_esc="${SONG_NAME// /\\ }"
	local exports_dir_esc="${PROJECT__EXPORTS_RELDIR// /\\ }"

	for score_name in "${score_names[@]}"; do
		[[ -n $verbose ]] && echo -en "\r$progress_message [$i/$nb_scores]" >&2
		(( i++ ))

		local score_deps
		readarray -t score_deps < <(get_score_deps "$score_name")

		local score_name_esc="${score_name// /\\ }"
		local score_export_dir_esc="$exports_dir_esc/$score_name_esc"
		local score_basename_esc="${score_export_dir_esc}/${song_name_esc}\\ -\\ ${score_name_esc}"

		local mp3_file_esc="${score_basename_esc}.mp3"

		local dep

		local targets target target_esc midi_pdf_targets mp3_targets
		readarray -t targets < <(get_output_filenames "$score_name")
		midi_pdf_targets=()
		mp3_targets=()
		for target in "${targets[@]}"; do
			target_esc="$exports_dir_esc/${target// /\\ }"
			if [[ $target =~ \.(midi|pdf)$ ]]; then
				midi_pdf_targets+=( "$target_esc" )
			elif [[ $target =~ \.mp3$ ]]; then
				mp3_targets+=( "$target_esc" )
			fi
			all_targets+=( "$target_esc" )
		done
		all_midi_pdf_targets+=( "${midi_pdf_targets[@]}" )

		if [[ ${#midi_pdf_targets[@]} != 0 ]]; then
			for target_esc in "${midi_pdf_targets[@]}"; do
				echo -n "$target_esc "
				if [[ $target_esc =~ \.midi$ ]]; then
					midi_file_esc="$target_esc"
				fi
			done
			echo -n ":"
			for dep in "${score_deps[@]}"; do
				echo -n " ${dep// /\\ }"
			done
			echo -e "\n\t@ ./manage build pdf-midi \"$score_name\"\n"
		fi

		if [[ ${#mp3_targets[@]} != 0 ]]; then
			for target_esc in "${mp3_targets[@]}"; do
				echo -n "$target_esc "
			done
			echo -n ":"
			echo " $midi_file_esc"
			echo -e "\t@ ./manage build mp3 \"$score_name\""
			echo
		fi
	done

	echo ".PHONY: all"
	echo -n "all:"
	for target in "${all_targets[@]}"; do
		echo -n " $target"
	done
	echo -e "\n"

	echo ".PHONY: pdf_midi"
	echo -n "pdf_midi:"
	for target in "${all_midi_pdf_targets[@]}"; do
		echo -n " $target"
	done
	echo -e "\n"

	[[ -n $verbose ]] && echo >&2
}

#-------------------- command_complete()
command_complete()
{
	:
}
