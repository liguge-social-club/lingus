#-------------------- command_help()
command_help()
{
    echo "show scores   Prints the list of all score names"
	echo "show outputs  Prints the list of all output filenames"
}

#-------------------- command_exec()
command_exec()
{
	case $1 in
		scores) get_score_names ;;
		outputs) get_all_output_filenames ;;
		*) fatal "Unrecognized sub-command: $1"
	esac
}

#-------------------- command_complete()
command_complete()
{
	echo "scores"
}
