#-------------------- command_help()
command_help()
{
    echo "deps <LILYPOND_FILE>  Shows all lilypond local dependencies for the given file"
}

#-------------------- command_exec()
command_exec()
{
	get_lily_deps "$@"
}

#-------------------- command_complete()
command_complete()
{
	:
}
