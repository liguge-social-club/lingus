ENTR=/usr/bin/entr

#-------------------- command_help()
command_help()
{
    echo "watch [WHAT [SCORE...]]  Watches changes in the dependencies of the given"
    echo "                         score(s) and builds the output files when needed."
    echo "Arguments:"
    echo "  WHAT  Can be the following keywords:"
    echo "         pdf|midi|pdf-midi  Build only pdf and midi"
    echo "         mp3|all|-          Build everything including mp3 (default)"
    echo "  SCORE  The name of score to watch (score names can be listed with the commmand"
    echo "         './manage show scores'). If not given, then watch all the existing scores."
    echo "Required binary: $ENTR"
}

#-------------------- command_exec()
command_exec()
{
    need_binaries $ENTR

    local make_targets=()
    local subsub_command="$1"
    shift

    local score_name
    local dep score_deps all_deps=()

    for score_name in "$@"; do
        case $subsub_command in
            pdf|midi|pdf-midi)
                make_targets+=( "$(get_relative_pdf_file "$score_name")" )
                ;;
            mp3|all|'-'|'')
                make_targets+=( "$(get_relative_mp3_file "$score_name")" )
                ;;
            *)
                fatal "Command watch: Unrecognized sub-command: ${bold}${subsub_command}${reset}"
        esac

        readarray -t score_deps < <(get_score_deps "$score_name")
        all_deps+=( "${score_deps[@]}" )
    done

    if [[ ${#all_deps[@]} != 0 ]]; then
        for dep in "${all_deps[@]}"; do
            echo "$dep"
        done | sort -u
    else
        get_all_lily_deps
    fi \
    | if [[ ${#make_targets[@]} == 0 ]]; then
        case $subsub_command in
            pdf|midi|pdf-midi) $ENTR make pdf_midi ;;
            *)                 $ENTR make all ;;
        esac
    else
        $ENTR make "${make_targets[@]}"
    fi
}

#-------------------- command_complete()
command_complete()
{
    get_score_names
}
