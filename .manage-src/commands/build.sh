#-------------------- command_help()
command_help()
{
    echo "build [-p] <WHAT> [SCORE]  Builds some output file."
    echo "Options:"
    echo "  -p  Build each score in a parallel separate job"
    echo "      WARNING: experimental => don't hit Ctrl-C (TODO: catch signals)"
}

#-------------------- command_exec()
command_exec()
{
    local build_parallel=
    if [[ $1 == -p ]]; then
        build_parallel=1
        shift
    fi

    local subsub_command="$1"
    shift
    export GENERATE_PDF_MIDI=0 GENERATE_MP3=0

    case $subsub_command in
        pdf|midi|pdf-midi)
            GENERATE_PDF_MIDI=1
            ;;
        mp3)
            GENERATE_MP3=1
            ;;
        all|'-'|'')
            GENERATE_PDF_MIDI=1
            GENERATE_MP3=1
            ;;
        *)
            fatal "Command build: Unrecognized sub-command: ${bold}${subsub_command}${reset}"
    esac

    local score_names score_name
    if [[ -n $1 ]]; then
        score_names=( "$1" )
    else
        readarray -t score_names < <(get_score_names)
    fi

    local lilypond_pids
    lilypond_pids=()

    for score_name in "${score_names[@]}"; do
        if [[ -n $build_parallel ]]; then
            (
                echo "Start building files for score \"$score_name\"..."
                build_score "$score_name" &>/dev/null
                echo -e "\rFinished building files for score \"$score_name\"."
            ) &
            lilypond_pids+=( $! )
        else
            build_score "$score_name"
        fi
    done

    if [[ -n $build_parallel ]]; then
        (
            local chars
            chars=( '-' '\' '|' '/' )
            while true; do
                local delay=0.1
                for c in "${chars[@]}"; do
                    echo -ne "\r$c "
                    sleep $delay
                done
            done
        ) &
        local progress_char_pid=$!
        wait "${lilypond_pids[@]}"
        kill $progress_char_pid
    fi
}

#-------------------- command_complete()
command_complete()
{
    get_score_names
}
