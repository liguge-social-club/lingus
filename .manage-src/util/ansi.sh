
# Define some color CSI codes:

CSI=$'\e['

reset="${CSI}0m"
bold="${CSI}1m"
italic="${CSI}3m"
underline="${CSI}4m"

black="${CSI}30m"
red="${CSI}31m"
green="${CSI}32m"
yellow="${CSI}33m"
blue="${CSI}34m"
magenta="${CSI}35m"
cyan="${CSI}36m"
white="${CSI}37m"
darkgrey="${CSI}30;1m"

black_bg="${CSI}40m"
red_bg="${CSI}41m"
green_bg="${CSI}42m"
yellow_bg="${CSI}43m"
blue_bg="${CSI}44m"
magenta_bg="${CSI}45m"
cyan_bg="${CSI}46m"
white_bg="${CSI}47m"

#-------------------- rgb()
rgb()
{
	echo -e "${CSI}38;2;$1;$2;$3m"
}

