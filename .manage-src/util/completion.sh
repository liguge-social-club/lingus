
#-------------------- do_completion()
do_completion()
{
	local cur="$1"
	local prev="$2"
	local cword="$3"
	shift 3
	local words=( "$@" )

	# Parse cli arguments:
	local command_args=() arg cli_command= command_index
	for i in $( seq $((cword - 1)) ); do
		arg="${words[$i]}"

		# Define the command:
		if [[ -z $cli_command && $arg != -* ]]; then
			command_index=$i
			cli_command="$arg"
			continue
		fi

		# Command arguments:
		if [[ -n $cli_command ]]; then
			command_args+=( "$arg" )
			continue
		fi

		# Global options:
		case $arg in
			-q|--quiet)
				verbose=0
				;;
		esac
	done

	if [[ -z $cli_command ]]; then
		get_commands
		echo '--help --quiet'
		return
	fi

	is_command "$cli_command" || return
	source_command "$cli_command"
	command_complete "$cur" "$prev" $((cword - command_index)) "${command_args[@]}"
}
