
#-------------------- _handle_short_paras()
_handle_short_paras()
{
	local found_one para short_para
	# On enlève le tiret '-' et on sépare en caractères:
	local paras=$(echo ${1##-} | grep -o .)

	for para in $paras; do
		found_one=
		for short_para in ${!short_paras[@]}; do
			if [[ $para == $short_para ]]; then
				_argv+=( --${short_paras[$para]} )
				found_one=1
			fi
		done
		[[ -z $found_one ]] && error "Paramètre non-reconnu: -$para"
	done
}


#-------------------- _shift_array()
_shift_array()
{
	shift
	echo "$@"
}


#-------------------- _translate_short_paras()
_translate_short_paras()
{
	local old_argv=( "${_argv[@]}" )
	_argv=()

	while [[ ${#old_argv} > 0 ]]; do
		arg="${old_argv[0]}"
		if [[ $arg =~ ^-[[:alpha:]]+$ ]]; then
			_handle_short_paras "$arg"
		else
			_argv+=( "$arg" )
		fi
		old_argv=( "${old_argv[@]:1}" )
	done
}


#-------------------- _reset_boolean_paras()
_reset_paras()
{
	local para

	for para in "${boolean_paras[@]}" "${needed_arg_paras[@]}"; do
		local var_name=${para//-/_}
		eval "$var_name="
	done
}


#-------------------- _handle_boolean_paras()
_handle_boolean_paras()
{

	[[ ! $1 =~ --.* ]] && return 1

	local opt_name=${1##--}
	local boolean_para

	for boolean_para in "${boolean_paras[@]}"; do
		[[ $boolean_para != $opt_name ]] && continue
		local var_name=${opt_name//-/_}
		eval "$var_name=1"
		return 0
	done

	return 1
}


#-------------------- _handle_needed_arg_para()
_handle_needed_arg_para()
{
	[[ ! $1 =~ --.* ]] && return 1

	local opt_name=${1##--}
	local needed_arg_para

	for needed_arg_para in ${needed_arg_paras[@]}; do
		[[ $opt_name != $needed_arg_para ]] && continue
		[[ -z $2 ]] && error "Option --$opt_name : paramètre requis"
		local var_name=${opt_name//-/_}
		eval "$(printf "%s=%q" $var_name "$2")"
		return 0
	done

	return 1
}


#-------------------- _handle_translated_paras()
_handle_translated_paras()
{
	_reset_paras

	while [[ $# != 0 ]]; do
		_handle_boolean_paras "$1" && { shift; continue; }
		_handle_needed_arg_para "$@" && { shift; shift; continue; }
		[[ -n $_handle_other_paras ]] && $_handle_other_paras "$@"

		shift
	done
}


#-------------------- _init_paras()
_init_paras()
{
	_argv=( "$@" )
}


#-------------------- _handle_paras()
_handle_paras()
{
	_handle_other_paras=$1
	shift
	_init_paras "$@"
	_translate_short_paras
	_handle_translated_paras "${_argv[@]}"
}

