#-------------------- debug()
debug()
{
	echo "${black}${bold}$@${reset}" >&2
}

#-------------------- debug_args()
debug_args()
{
	for arg in "$@"; do
		debug "- $arg"
	done
}
