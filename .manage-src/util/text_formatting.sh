
# Indentation:
indent=
indent_level=0
indent_width=3

#-------------------- repeat()
# Repeat $1 times the character $2.
repeat()
{
    printf -- "$2%.0s" $(seq 1 $1)
}

#-------------------- title()
title()
{
    echo_indent "$yellow$@$reset"
}

#-------------------- echo_color()
echo_color()
{
    echo "$2$1${reset}"
}

#-------------------- echo_indent()
echo_indent()
{
    echo "$indent$@"
}

#-------------------- indent()
indent()
{
    (( indent_level += 1 ))
    _set_indent
}

#-------------------- dedent()
dedent()
{
    (( indent_level -= 1 ))
    _set_indent
}

#-------------------- _set_indent()
_set_indent()
{
    indent="$(repeat $(( $indent_level * $indent_width )) ' ')"
}

