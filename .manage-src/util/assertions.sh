
#-------------------- fatal()
# Display an error, then quit
fatal()
{
	echo "${red}Error${reset}: $@" >&2
	exit 1
}

#-------------------- variables_must_be_defined()
# Check if the given variables are all not empty.
variables_must_be_defined()
{
	for var_name in "$@"; do
		[[ -z ${!var_name} ]] && fatal "Variable ${bold}\$${var_name}${reset} is not defined."
	done
}


#-------------------- need_binaries()
# Raise an error if the given binaries don't exist.
need_binaries()
{
	for binary in "$@"; do
		[[ -x $binary ]] || fatal "Binary ${bold}${binary}${reset} is needed"
	done
}

