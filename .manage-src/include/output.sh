#-------------------- build_score()
build_score()
{
	set -e

    local tmp_dir
    tmp_dir=$(mktemp -d)
    [[ -n $tmp_dir ]] || fatal "Erreur interne"

    local score="$1"
    is_score_name "$score" || fatal "'$score' is not a score name"

    local separate_dirs=1

    trap "
        rm -r \""$tmp_dir"\"
        trap - EXIT
        exit
    " EXIT SIGINT SIGTERM

    local score_export_dir="$PROJECT__EXPORTS_DIR/$score"
	[[ -n $2 ]] && score_export_dir="$2/$score"
    mkdir -p "$score_export_dir"

    if [[ $GENERATE_PDF_MIDI == 1 || $GENERATE_MP3 == 1 ]]; then
        local lilypond_success=
        (
            cd "$tmp_dir"
            export SONG_NAME
            lilypond "$PROJECT__SCORES_DIR/$score.ly"
        ) && lilypond_success=1

        if [[ -n $separate_dirs ]]; then
            mkdir -p "$score_export_dir/midi" "$score_export_dir/pdf"
        fi

        if [[ -n $lilypond_success ]]; then
			[[ -n $tmp_dir ]] || fatal "Erreur interne"
            mv "$tmp_dir"/* "$score_export_dir"

            # Correction du nom de fichier midi si nécessaire:
            local false_midi_file=$(find "$score_export_dir" -name '*-1.midi')
            if [[ -n $false_midi_file ]]; then
                local corrected_midi_file="${false_midi_file%%-1.midi}.midi"
                echo "Correction: déplacement de '$false_midi_file' vers '$corrected_midi_file'"
                mv "$false_midi_file" "$corrected_midi_file"
            fi
        fi
    fi

    if [[ $GENERATE_MP3 == 1 ]]; then
        (
            set -eu
            local temp_wav_file="$tmp_dir/__temp__.wav"
            local mp3_file
            if [[ -n $separate_dirs ]]; then
                mkdir -p "$score_export_dir/mp3"
            fi
            while read midi_file; do
                mp3_file="${midi_file%.midi}.mp3"
                timidity "$midi_file" -Ow -o "$temp_wav_file"
                lame "$temp_wav_file" "$mp3_file"
            done < <(find "$score_export_dir" -name '*.midi')
            rm "$temp_wav_file" 2>/dev/null
        )
    fi

    if [[ -n $separate_dirs ]]; then
        (
            echo "Déplacement des fichiers dans des dossiers séparés"
            cd "$score_export_dir"
            mv -f *.pdf pdf
            mv -f *.midi midi
            if [[ $GENERATE_MP3 == 1 ]]; then
                mv -f *.mp3 mp3
            fi
        )
    fi
}

#-------------------- get_output_filenames()
get_output_filenames()
{
    (
        set -e

        local tmp_dir
        tmp_dir=$(mktemp -d)
        [[ -n $tmp_dir ]] || fatal "Erreur interne"

        local score_name="$1"

        export GENERATE_PDF_MIDI=1
        build_score "$score_name" "$tmp_dir" >/dev/null 2>&1

        local dir_len=$(( ${#tmp_dir} + 1 ))
        local file
        while read file; do
            file="${file:$dir_len}"
            echo "$file"
            if [[ $file =~ .midi$ ]]; then
                echo "${file%%.midi}.mp3"
            fi
        done < <(find "$tmp_dir" -type f)

        rm "$tmp_dir" -r
    )
}

#-------------------- get_all_output_filenames()
get_all_output_filenames()
{
    (
        set -e

        local tmp_dir
        tmp_dir=$(mktemp -d)
        [[ -n $tmp_dir ]] || fatal "Erreur interne"

        local score_names score_name nb_scores i
        readarray -t score_names < <(get_score_names)
        nb_scores=${#score_names[@]}
        i=1
        export GENERATE_PDF_MIDI=1
        for score_name in "${score_names[@]}"; do
            echo -ne "\rAnalyse des partitions... [$i/$nb_scores]" >&2
            (( i++ ))
            build_score "$score_name" "$tmp_dir" >/dev/null 2>&1
        done
        echo -n "\nOk." >&2

        local dir_len=$(( ${#tmp_dir} + 1 ))
        local file
        while read file; do
            file="${file:$dir_len}"
            echo "$file"
            if [[ $file =~ .midi$ ]]; then
                echo "${file%%.midi}.mp3"
            fi
        done < <(find "$tmp_dir" -type f)

        rm "$tmp_dir" -r
    )
}
