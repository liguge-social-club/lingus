#-------------------- get_score_names()
get_score_names() {
	local score_name

	for lilypond_file in "$PROJECT__SCORES_DIR"/*.ly; do
		score_name="$(basename "$lilypond_file")"
		score_name="${score_name%%.ly}"
		echo "$score_name"
	done
}

#-------------------- get_score_file()
get_score_file()
{
	echo -n "$PROJECT__SCORES_DIR/$1.ly"
}

#-------------------- get_relative_score_file()
get_relative_score_file()
{
	echo -n "$PROJECT__SCORES_RELDIR/$1.ly"
}

#-------------------- is_score_name()
is_score_name() {
	! [[ $1 =~ [.~/] ]] && [[ -f "$PROJECT__SCORES_DIR/$1.ly" ]]
}
