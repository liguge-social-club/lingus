#-------------------- remove_export_tmp_files()
remove_export_tmp_files()
{
	[[ -d $PROJECT__EXPORTS_DIR ]] || return
	rm -rf "$PROJECT__EXPORTS_DIR"/tmp.*
}

#-------------------- get_pdf_file()
get_pdf_file()
{
	local score_name="$1"
	echo -n "$PROJECT__EXPORTS_DIR/$score_name/$SONG_NAME - $score_name.pdf"
}

#-------------------- get_midi_file()
get_midi_file()
{
	local score_name="$1"
	echo -n "$PROJECT__EXPORTS_DIR/$score_name/$SONG_NAME - $score_name.midi"
}

#-------------------- get_mp3_file()
get_mp3_file()
{
	local score_name="$1"
	echo -n "$PROJECT__EXPORTS_DIR/$score_name/$SONG_NAME - $score_name.mp3"
}

#-------------------- get_relative_pdf_file()
get_relative_pdf_file()
{
	local score_name="$1"
	echo -n "$PROJECT__EXPORTS_RELDIR/$score_name/$SONG_NAME - $score_name.pdf"
}

#-------------------- get_relative_midi_file()
get_relative_midi_file()
{
	local score_name="$1"
	echo -n "$PROJECT__EXPORTS_RELDIR/$score_name/$SONG_NAME - $score_name.midi"
}

#-------------------- get_relative_mp3_file()
get_relative_mp3_file()
{
	local score_name="$1"
	echo -n "$PROJECT__EXPORTS_RELDIR/$score_name/$SONG_NAME - $score_name.mp3"
}

#-------------------- get_all_mp3()
get_all_mp3()
{
	local score_name
	while read score_name; do
		echo "$(get_relative_mp3_file "$score_name")"
	done < <(get_score_names)
}

#-------------------- get_all_pdf()
get_all_pdf()
{
	local score_name
	while read score_name; do
		echo "$(get_relative_pdf_file "$score_name")"
	done < <(get_score_names)
}
