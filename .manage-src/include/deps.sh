__all_deps=()

#-------------------- get_lily_deps()
get_lily_deps()
{
	local base_file="$1"
	[[ -f $base_file ]] || fatal "'$base_file' is not a file"

	local base_dir="$( cd "$(dirname "$(realpath "$base_file")" )" && pwd )"

	while read include_file; do
		local file=$(realpath "$base_dir/$include_file")
		file=${file##$PROJECT_DIR/}

		local skip=
		for d in "${__all_deps[@]}"; do
			if [[ "$d" == "$file" ]]; then
				skip=1
				break
			fi
		done
		[[ -n $skip ]] && continue

		echo "$file"
		get_lily_deps "$base_dir/$include_file"
	done < <(get_lily_level1_deps "$base_file")
}

#-------------------- get_score_deps()
get_score_deps()
{
	local score_name="$1"
	local score_file=$(get_score_file "$score_name")
	[[ -f $score_file ]] || fatal "Score not found: '${bold}${score_file}${reset}'"
	local rel_score_file=$(get_relative_score_file "$score_name")
	echo "$rel_score_file"
	get_lily_deps "$score_file"
}

#-------------------- get_all_lily_deps()
get_all_lily_deps()
{
	__all_deps=()

	local score_name
	while read score_name; do
		local score_deps
		readarray -t score_deps < <(get_score_deps "$score_name")
		__all_deps+=( "${score_deps[@]}" )
	done < <(get_score_names)

	local dep
	for dep in "${__all_deps[@]}"; do
		echo "$dep"
	done | sort -u

	__all_deps=()
}

#-------------------- get_lily_level1_deps()
get_lily_level1_deps()
{
	cat "$1" | awk -F\" '/^\\include +"\./ { print $2 }'
}
