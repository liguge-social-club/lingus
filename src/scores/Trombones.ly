\version "2.18.2"

PART_NAME = "Trombones"
IS_CONDUCTOR = ##f

\include "../init.ily"
\include "../staves/trombones.ily"

music = \"Staff - Trombones"
midiMusic = \"Staff - Trombones - Midi"

\include "../output/part.ily"
\include "../output/midi.ily"
\include "../output/midi_with_ticks.ily"
