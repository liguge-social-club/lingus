\version "2.18.2"

PART_NAME = "Conducteur"
IS_CONDUCTOR = ##t

\include "../init.ily"
\include "../staves/clarinettes.ily"
\include "../staves/trompettes.ily"
\include "../staves/trombones.ily"
\include "../staves/sax alto.ily"
\include "../staves/sax bar.ily"
\include "../staves/tuba basse.ily"
\include "../staves/drums.ily"

music = <<
  \"Staff - Clarinettes"
  \"Staff - Trompettes"
  \"Staff - Trombones"
  \"Staff - Saxophone alto"
  \"Staff - Saxophone baryton"
  \"Staff - Tuba basse"
>>
midiMusic = <<
  \"Staff - Clarinettes - Midi"
  \"Staff - Trompettes - Midi"
  \"Staff - Trombones - Midi"
  \"Staff - Saxophone alto"
  \"Staff - Saxophone baryton - Midi"
  \"Staff - Tuba basse - Midi"
  \"Staff - Drums"
>>

\include "../output/conducteur.ily"
\include "../output/midi.ily"
\include "../output/midi_with_ticks.ily"
