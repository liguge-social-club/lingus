\version "2.18.2"

PART_NAME = "Saxophone baryton"
IS_CONDUCTOR = ##f

\include "../init.ily"
\include "../staves/sax bar.ily"

music = \"Staff - Saxophone baryton"
midiMusic = \"Staff - Saxophone baryton - Midi"

\include "../output/transpo-mi-b.ily"
\include "../output/midi.ily"
\include "../output/midi_with_ticks.ily"
