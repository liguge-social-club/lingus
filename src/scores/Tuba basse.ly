\version "2.18.2"

PART_NAME = "Tuba basse"
IS_CONDUCTOR = ##f

\include "../init.ily"
\include "../staves/tuba basse.ily"

music = \"Staff - Tuba basse"
midiMusic = \"Staff - Tuba basse - Midi"

\include "../output/part.ily"
\include "../output/transpo-si-b.ily"
\include "../output/transpo-mi-b.ily"
\include "../output/midi.ily"
\include "../output/midi_with_ticks.ily"
