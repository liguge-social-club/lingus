\version "2.18.2"

PART_NAME = "Saxophone alto"
IS_CONDUCTOR = ##f

\include "../init.ily"
\include "../staves/sax alto.ily"

music = \transpose c c' \"Staff - Saxophone alto"
midiMusic = \transpose c c' \"Staff - Saxophone alto - Midi"

\include "../output/transpo-si-b.ily"
\include "../output/transpo-mi-b.ily"
\include "../output/midi.ily"
\include "../output/midi_with_ticks.ily"
