\version "2.18.2"

PART_NAME = "Soubassophone"
IS_CONDUCTOR = ##f

\include "../init.ily"
\include "../staves/soubassophone.ily"

music = \transpose c c'' \"Staff - Soubassophone"
midiMusic = \"Staff - Soubassophone - Midi"

\include "../output/transpo-si-b.ily"
\include "../output/midi.ily"
