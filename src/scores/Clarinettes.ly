\version "2.18.2"

PART_NAME = "Clarinettes"
IS_CONDUCTOR = ##f

\include "../init.ily"
\include "../staves/clarinettes.ily"

music = \"Staff - Clarinettes"
midiMusic = \"Staff - Clarinettes - Midi"

\include "../output/transpo-si-b.ily"
\include "../output/midi.ily"
\include "../output/midi_with_ticks.ily"
