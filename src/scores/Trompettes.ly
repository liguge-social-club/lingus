\version "2.18.2"

PART_NAME = "Trompettes"
IS_CONDUCTOR = ##f

\include "../init.ily"
\include "../staves/trompettes.ily"

music = \"Staff - Trompettes"
midiMusic = \"Staff - Trompettes - Midi"

\include "../output/transpo-si-b.ily"
\include "../output/midi.ily"
\include "../output/midi_with_ticks.ily"
