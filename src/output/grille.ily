\paper {
  markup-system-spacing = #'(
    (basic-distance . 0)
    (minimum-distance . 0)
    (padding . 8)
    (stretchability . 12)
  )
  system-system-spacing = #'(
    (basic-distance . 0)
    (minimum-distance . 0)
    (padding . 1)
    (stretchability . 1)
  )
  indent = 0
}

\book {
  \bookOutputName #(format "~a - ~a" SONG_NAME PART_NAME)
  \score {
    \music
    \layout {
      \context {
        \Score
        \omit Clef
        \omit TimeSignature
        \omit BarNumber
        \override SpacingSpanner.strict-note-spacing = ##t
        proportionalNotationDuration = #(ly:make-moment 5/8)
        \override RehearsalMark.Y-offset = #0
      }
      \context {
        \Staff
        \remove "Instrument_name_engraver"
        \remove "Staff_symbol_engraver"
      }
      \context {
        \ChordNames
        \override ChordName.extra-offset = #'(3 . -1)
        \override ChordName.Y-offset = #0
        \remove "Instrument_name_engraver"
        \remove "Clef_engraver"
        \consists "Staff_symbol_engraver"
        \override StaffSymbol.line-positions = #'(-6 6)
        \consists "Bar_engraver"
        \consists "Percent_repeat_engraver"
      }
    }
  }
}
