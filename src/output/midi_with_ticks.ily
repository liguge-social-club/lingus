\book {
  \bookOutputName #(format "~a - ~a + Métronome à la noire" SONG_NAME PART_NAME)
  \score {
    \articulate \addMidiTicks 5 4 \midiMusic
    \midi {}
  }
}

\book {
  \bookOutputName #(format "~a - ~a + Métronome à la blanche" SONG_NAME PART_NAME)
  \score {
    \articulate \addMidiTicks 5 2 \midiMusic
    \midi {}
  }
}
