\include "../style/part.ily"

\book {
  \bookOutputName #(format "~a - ~a" SONG_NAME PART_NAME)
  \score {
    \new StaffGroup \music
    \layout {
      \context {
        \Score
        \override SpacingSpanner.common-shortest-duration = #(ly:make-moment 1/8)
      }
    }
  }
}
