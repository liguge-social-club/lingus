\paper {
  markup-system-spacing = #'(
    (basic-distance . 20)
    (minimum-distance . 6)
    (padding . 1)
    (stretchability . 12)
  )
  system-system-spacing = #'(
    (basic-distance . 18)
    (minimum-distance . 6)
    (padding . 1)
    (stretchability . 12)
  )
  left-margin = #25
}

\book {
  \bookOutputName #(format "~a - ~a" SONG_NAME PART_NAME)
  \score {
    \new StaffGroup \music
    \layout {}
  }
}

\book {
  \bookOutputName #(format "~a - ~a (Transpo Mi-b)" SONG_NAME PART_NAME)
  \header {
      instrument = #(format "~a - Transpo en Mi♭" PART_NAME)
  }
  \score {
    \new StaffGroup \transpose ef c \music
    \layout {}
  }
}

\book {
  \bookOutputName #(format "~a - ~a (Transpo Si-b)" SONG_NAME PART_NAME)
  \header {
      instrument = #(format "~a - Transpo en Si♭" PART_NAME)
  }
  \score {
    \new StaffGroup \transpose bf c' \music
  }
}
