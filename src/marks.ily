m = #(define-music-function (text) (string?) #{
  \mark \markup {
    \override #'(box-padding . 0.5)
    \box
    \bold
    #text
  }
#})

"rehearsal marks" = \new Voice \with {
  \remove "Multi_measure_rest_engraver"
} {
    \m "Intro"
    s1*5/4 * 4
    \m "Thème (1)"
    s1*5/4 * 16
    \m "Pré-refrain speed (1)"
    s1*5/4 * 6
    \m "Refrain (1)"
    s1*5/4 * 8
    \m "Thème (2)"
    s1*5/4 * 16
    \m "Pré-refrain speed (2)"
    s1*5/4 * 14
    \m "Refrain (2)"
    s1*5/4 * 8
    \m "Refrain (2')"
    s1*5/4 * 8
}
