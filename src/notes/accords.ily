"Accords - Refrain - Voix 1" = \relative c {
  e2.~ 2
  d2.~ 2
  d2.~ 2
  e2.~ 2
  e2~ 8  e~ 2
  d2~ 8  d~ 2
  d2~ 8  d~ 2
  b2~ 8  c~ 2
}

"Accords - Refrain - Voix 2" = \relative c' {
  b2.~ 2
  c2.~ 2
  c2.~ 2
  c2.~ 2
  b2~ 8  b~ 2
  c2~ 8  c~ 2
  c2~ 8  c~ 2
  b2~ 8  c~ 2
}

"Accords - Refrain - Voix 3" = \relative c' {
  a2.~  2
  g2.~  2
  fs2.~ 2
  g2.~  2
  a2~   8  af~ 2
  g2~   8  fs~ 2
  fs2~  8  e~  2
  g2~   8  f~  2
}

"Accords - Refrain B' - Voix 1" = \relative c' {
  r2 r8 e8~ 2
  r2 r8 d8~ 2
  r2 r8 d8~ 2
  r2 r8 c8~ 2
  r2 r8 e8~ 2
  r2 r8 d8~ 2
  r2 r8 cs8~ 2
  r2 r8 c8~ 2
}

"Accords - Refrain B' - Voix 2" = \relative c' {
  r2 r8 b8~ 2
  r2 r8 a8~ 2
  r2 r8 a8~ 2
  r2 r8 a8~ 2
  r2 r8 b8~ 2
  r2 r8 a8~ 2
  r2 r8 a8~ 2
  r2 r8 a8~ 2
}

"Accords - Refrain B' - Voix 3" = \relative c' {
  r2 r8 a8~ 2
  r2 r8 g8~ 2
  r2 r8 fs8~ 2
  r2 r8 f8~ 2
  r2 r8 gs8~ 2
  r2 r8 fs8~ 2
  r2 r8 g8~ 2
  r2 r8 f8~ 2
}

% "Accords - Refrain - Voix 3" = \relative c {
%   e2.~ 2
%   c2.~ 2
%   d2.~ 2
%   a2.~ 2
%   e2.~ 2
%   c2.~ 2
%   d2.~ 2
%   g2~  8 f~ 2
% }
