\include "./intro.ily"
\include "./gimmick 1.ily"
\include "./gimmick 2.ily"
\include "./refrain.ily"

"Drums - Kick" = \drummode {
  % Intro
  \"Intro - Silence"

  % Gimmick 1
  \repeat unfold 8 {
    bd4 r r r bd
    R1*5/4
  }

  % Gimmick 2
  \repeat unfold 6 {
    bd4 r4. bd4 bd bd8
  }

  % Refrain
  R1*5/4 * 8

  % Gimmick 1b
  \repeat unfold 8 {
    bd4 r r r bd
    R1*5/4
  }

  % Gimmick 2b
  \repeat unfold 14 {
    bd4 r4. bd4 bd bd8
  }

  % Refrain B
  R1*5/4 * 4
  \repeat unfold 4 {
    bd4 4 4 4 4
  }

  % Refrain B' - Tr
  r2 r8 bd8 r2
  \repeat unfold 7 {
    bd8. 8. 4 8 r2
  }
}

"Drums - Hit hat" = \drummode {
  % Intro
  \"Intro - Silence"

  % Gimmick 1
  \repeat unfold 16 {
    hh8 hhp hhp hhc hh hhc hh hhc hh hhc
  }

  % Gimmick 2
  \repeat unfold 6 {
    hh8 hhp16 hhp
    hh8 hhp16 hhp
    hh8 hhp16 hhp
    hh8 hhp16 hhp
    hh8 hhp16 hhp
  }

  % Refrain
  R1*5/4 * 8

  % Gimmick 1b
  \repeat unfold 16 {
    hh8 hhp hhp hhc hh hhc hh hhc hh hhc
  }

  % Gimmick 2b
  \repeat unfold 14 {
    hh8 hhp16 hhp
    hh8 hhp16 hhp
    hh8 hhp16 hhp
    hh8 hhp16 hhp
    hh8 hhp16 hhp
  }

  % Refrain B
  \"Refrain - Silence"

  % Refrain B' - Tr
  r2 r8
  r8 hhp16 hhp
  hh8 hhp16 hhp
  hh8 hhp16 hhp
  \repeat unfold 7 {
    hh8 hhp16 hhp
    hh8 hhp16 hhp
    hh8 hhp16 hhp
    hh8 hhp16 hhp
    hh8 hhp16 hhp
  }
}

"Drums - Shake" = \drummode {
  % Intro
  \"Intro - Silence"

  % Gimmick 1
  \repeat unfold 8 {
    r4 mar r mar r mar r mar r mar
  }

  % Gimmick 2
  R1*5/4 * 6

  % Refrain
  R1*5/4 * 8

  % Gimmick 1b
  \repeat unfold 8 {
    r4 mar r mar r mar r mar r mar
  }

  % Gimmick 2b
  R1*5/4 * 14
}

"Drums - Snare" = \drummode {
  % Intro
  \"Intro - Silence"

  % Gimmick 1
  \repeat unfold 8 {
    r4 r sna r r
    r sna r r r
  }

  % Gimmick 2
  \repeat unfold 6 {
    r4 r sna r r
  }

  % Refrain
  R1*5/4 * 8

  % Gimmick 1b
  \repeat unfold 8 {
    r4 r sna r r
    r sna r r r
  }

  % Gimmick 2b
  \repeat unfold 14 {
    r4 r sna r r
  }

  % Refrain B
  R1*5/4 * 8

  sna8 sna16 sna16 r16 sna16 sna8 r2.
}

"Drums - Toms" = \drummode {
  % Intro
  \"Intro - Silence"

  % Gimmick 1
  \repeat unfold 2 {
    R1*5/4
    r4 r r tommh8 tomml tomml toml
    R1*5/4
    r4 r r tomml8 tomml tomml toml
    R1*5/4
    r4 r r tommh8 tomml tomml r
    R1*5/4
    r4 r r tomml8 tomml tomml toml
  }

  % Gimmick 2
  R1*5/4 * 6

  % Refrain
  R1*5/4 * 8

  % Gimmick 1b
  \repeat unfold 2 {
    R1*5/4
    r4 r r tommh8 tomml tomml toml
    R1*5/4
    r4 r r tomml8 tomml tomml toml
    R1*5/4
    r4 r r tommh8 tomml tomml r
    R1*5/4
    r4 r r tomml8 tomml tomml toml
  }

  % Gimmick 2b
  R1*5/4 * 14
 * 8
}

"Drums - Crash" = \drummode {
  % Intro
  \"Intro - Silence"

  % Gimmick 1
  \repeat unfold 2 {
    cymc4\mf r2 r2
    R1*5/4 * 3
    cymc4\mf r2 r2
    R1*5/4 * 3
  }

  % Gimmick 2
  cymc4\mf r2 r2
  R1*5/4 * 3
  cymc4\mf r2 r2
  R1*5/4

  % Refrain
  cymc4 r2 r
  R1*5/4 * 7

  % Gimmick 1b
  \repeat unfold 2 {
    cymc4\mf r2 r2
    R1*5/4 * 3
    cymc4\mf r2 r2
    R1*5/4 * 3
  }

  % Gimmick 2b
  cymc4\mf r2 r2
  R1*5/4 * 3
  cymc4\mf r2 r2
  R1*5/4 * 3
  cymc4\mf r2 r2
  R1*5/4 * 3
  cymc4\mf r2 r2
  R1*5/4

  % Refrain B
  R1*5/4 * 4
  \repeat unfold 3 {
    cyms4\ppp 4 4 4 4
  }
  cyms4\< 4 4 4 4\ffff

  % Refrain B' - Tr
  r2 r8 cymc2\mf r8
}

"Drums - Ride" = \drummode {
  \"Intro - Silence"
  \"Gimmick 1 - Silence"
  \"Gimmick 2 - Silence"
  \"Refrain - Silence"
  \"Gimmick 1 - Silence"
  \"Gimmick 2b - Silence"
  \"Refrain - Silence"

  % Refrain B' - Tr
  R1*5/4
  \repeat unfold 7 {
    cymr4 cymr cymr cymr cymr
  }
}
