"Refrain" = \relative c'' {
  \repeat unfold 2 {
    e8.-^ e16-. r8 e,-. g(   a-.) e'16-^( d b8-.) b(   a-.)
    a8.-^ a16-. r8 b-.  d-^( b-.) a-^(      g-.)  g-^( e-.)
    d8.-^ d16-. r8 b-.  d(   e-.) a16-^( g  e8-.) e(   d-.)
    d8.-^ d16-. r8 e-.  g-^( e-.) e16-^( d  b8-.) a(   g-.)
  }
}

"Refrain - Silence" = R1*5/4 * 8
"Refrain - Silence caché" = s1*5/4 * 8

"Refrain - Fin Gimmick 2 + Silence" = \relative c'' {
  e8. e16 r2 r
  R1*5/4 * 7
}

"Refrain B - Trompettes" = \relative c'' {
    R1*5/4 * 4
    <<
        {
            b1~\p b4
            a1~ a4
            a1~ a4
            g2~ g8 f~ f2
        } {
            e'1~\p e4
            d1~ d4
            d1~ d4
            s1*5/4
        } {
            s1*5/4 * 2
            cs1~\p cs4
            b2~ b8 a~ a2
        }
    >>
}

"Refrain B2 phrase simplifié - mes.1 v1" = \relative c' {
  r4. e8( g b-.) d-^( b-.) b-^( a-.)
}

"Refrain B2 phrase simplifié - mes.1 v2" = \relative c' {
  e'8.-^ e16-. r8 e,( g b-.) d-^( b-.) b-^( a-.)
}

"Refrain B2 phrase simplifié - mes.2-4" = \relative c' {
  a'8.-^ a16-. r8 b(  d b-.) a-^(      g-.)  g-^( e-.)
  d8.-^ d16-. r8 b(  d   e-.) g-^( e-.) e-^(   d-.)
  d8.-^ d16-. r8 e(  g e-.) d-^( b8-.) a-^(   g-.)
}

"Refrain B2 simplifié" = {
  \"Refrain B2 phrase simplifié - mes.1 v1"
  \"Refrain B2 phrase simplifié - mes.2-4"
  \"Refrain B2 phrase simplifié - mes.1 v2"
  \"Refrain B2 phrase simplifié - mes.2-4"
}

"Refrain B2 - Patate au début" = \relative c' {
  r4. e16->\f r r2.
  R1*5/4 * 3
}
