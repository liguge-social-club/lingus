"Basse - Gimmick 1 - Phrase de base" = \relative c, {
  | e8. r16 e8( g) a4 a8( b) g4
  | r2 r a8( b)
  | c4 b8( a g a b4) g
  | e r2 r
}

"Basse - Gimmick 1" = \relative c, {
  \repeat unfold 4 \"Basse - Gimmick 1 - Phrase de base"
}

"Basse - Gimmick 2 - a" = \relative c,, {
  | % Bar 17
  c'4 r4. d8 r e r g(
  | % Bar 18
  c,4) r4. d8 r a r d(
  | % Bar 19
  e4) r4. g8 r a r b(
  | % Bar 20
  d,4) r4. g8 r a r b
}

"Basse - Gimmick 2 - b" = \relative c, {
  | % Bar 21
  c4 r4. d8 r e r g(
  | % Bar 22
  c,4) r4. d8 r a r d
}

"Basse - Gimmick 2" = {
  | % Bar 17
  \"Basse - Gimmick 2 - a"
  | % Bar 21
  \"Basse - Gimmick 2 - b"
}

"Basse - Gimmick 2b" = {
  \"Basse - Gimmick 2 - a"
  \"Basse - Gimmick 2 - a"
  \"Basse - Gimmick 2 - a"
  \"Basse - Gimmick 2 - b"
}

"Basse - Refrain" = \relative c {
  | % Bar 23
  e1~ 4
  | % Bar 24
  c1~ 4
  | % Bar 25
  d1~ 4
  | % Bar 26
  a1~ 4

  | % Bar 27
  e'2~ 8 e8~ 2
  | % Bar 28
  c2~ 8  c8~ 2
  | % Bar 29
  a2~ 8  g8~ 2
  | % Bar 30
  g~  8  f8~ 2
}

"Basse - Refrain B'" = \relative c, {
  r2                   r8  e8~ 2
  c'8 r16 16 r8 16 r16 r8  d~  2
  d8  r16 16 r8 16 r16 r8  8~  2
  a8  r16 16 r8 16 r16 r8  8~  2

  e8  r16 16 r8 16 r16 r8  e'8~ 2
  c8  r16 16 r8 16 r16 r8  d8~  2
  d8  r16 16 r8 16 r16 r8  cs8~ 2
  a8  r16 16 r8 16 r16 r8  8~   2

  % e'2~ 8 e8~ 2
  % c2~ 8  c8~ 2
  % a2~ 8  g8~ 2
  % g~  8  f8~ 2
}
