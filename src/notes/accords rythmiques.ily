"Accords rythmiques 1 - Voix 1" = \relative c' {
  a16-. r a-. a-. r a-. a-. r a-. r a-. r a-. r a-. r a-. r a-. r
}
"Accords rythmiques 1 - Voix 2" = \relative c {
  d16-. r d-. d-. r d-. d-. r d-. r d-. r d-. r d-. r d-. r d-. r
}
"Accords rythmiques 1 - Voix 3" = \relative c {
  e16-. r e-. e-. r e-. e-. r e-. r e-. r e-. r e-. r e-. r e-. r
}

% "Accords rythmiques 1 - Voix 1" = \relative c' {
%   a16-. r r a-. r r a-. r a-. r r r a-. r r r a-. r a-. r
% }
% "Accords rythmiques 1 - Voix 2" = \relative c {
%   d16-. r r d-. r r d-. r d-. r r r d-. r r r d-. r d-. r
% }
% "Accords rythmiques 1 - Voix 3" = \relative c {
%   e16-. r r e-. r r e-. r e-. r r r e-. r r r e-. r e-. r
% }

"Accords rythmiques 2a - Voix 1" = \relative c {
  e16-. r e-. e-. r e-. e-. r e-. r e-. r e-. r e-. r e-. r e-. r
}
"Accords rythmiques 2a - Voix 2" = \relative c' {
  g16-. r g-. g-. r g-. g-. r g-. r g-. r g-. r g-. r g-. r g-. r
}
"Accords rythmiques 2a - Voix 3" = \relative c' {
  b16-. r b-. b-. r b-. b-. r b-. r b-. r b-. r b-. r b-. r b-. r
}

"Accords rythmiques 2b - Voix 1" = \relative c {
  d16-. r d-. d-. r d-. d-. r d-. r d-. r d-. r d-. r d-. r d-. r
}
"Accords rythmiques 2b - Voix 2" = \relative c {
  fs16-. r fs-. fs-. r fs-. fs-. r fs-. r fs-. r fs-. r fs-. r fs-. r fs-. r
}
"Accords rythmiques 2b - Voix 3" = \relative c' {
  b16-. r b-. b-. r b-. b-. r b-. r b-. r b-. r b-. r b-. r b-. r
}

"Accords rythmiques - Intro - Voix 1" = \relative c' {
  \repeatPercent 4 \"Accords rythmiques 1 - Voix 1"
}
"Accords rythmiques - Intro - Voix 2" = \relative c' {
  \repeatPercent 4 \"Accords rythmiques 1 - Voix 2"
}
"Accords rythmiques - Intro - Voix 3" = \relative c' {
  \repeatPercent 4 \"Accords rythmiques 1 - Voix 3"
}

"Accords rythmiques - Gimmick 1 - Voix 1" = \relative c' {
  \repeatPercent 16 \"Accords rythmiques 1 - Voix 1"
}
"Accords rythmiques - Gimmick 1 - Voix 2" = \relative c' {
  \repeatPercent 16 \"Accords rythmiques 1 - Voix 2"
}
"Accords rythmiques - Gimmick 1 - Voix 3" = \relative c' {
  \repeatPercent 16 \"Accords rythmiques 1 - Voix 3"
}

"Accords rythmiques - Gimmick 2A - Voix 1" = \relative c' {
  \repeatPercent 3 \"Accords rythmiques 2a - Voix 1"
  \"Accords rythmiques 2b - Voix 1"
  \repeatPercent 2 \"Accords rythmiques 2a - Voix 1"
}
"Accords rythmiques - Gimmick 2A - Voix 2" = \relative c' {
  \repeatPercent 3 \"Accords rythmiques 2a - Voix 2"
  \"Accords rythmiques 2b - Voix 2"
  \repeatPercent 2 \"Accords rythmiques 2a - Voix 2"
}
"Accords rythmiques - Gimmick 2A - Voix 3" = \relative c' {
  \repeatPercent 3 \"Accords rythmiques 2a - Voix 3"
  \"Accords rythmiques 2b - Voix 3"
  \repeatPercent 2 \"Accords rythmiques 2a - Voix 3"
}

"Accords rythmiques - Gimmick 2B - Voix 1" = \relative c' {
  \repeat unfold 3 {
    \repeatPercent 3 \"Accords rythmiques 2a - Voix 1"
    \"Accords rythmiques 2b - Voix 1"
  }
  \repeatPercent 2 \"Accords rythmiques 2a - Voix 1"
}
"Accords rythmiques - Gimmick 2B - Voix 2" = \relative c' {
  \repeat unfold 3 {
    \repeatPercent 3 \"Accords rythmiques 2a - Voix 2"
    \"Accords rythmiques 2b - Voix 2"
  }
  \repeatPercent 2 \"Accords rythmiques 2a - Voix 2"
}
"Accords rythmiques - Gimmick 2B - Voix 3" = \relative c' {
  \repeat unfold 3 {
    \repeatPercent 3 \"Accords rythmiques 2a - Voix 3"
    \"Accords rythmiques 2b - Voix 3"
  }
  \repeatPercent 2 \"Accords rythmiques 2a - Voix 3"
}
