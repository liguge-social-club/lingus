"Gimmick 1" = \relative c' {
  | % Bar 1
  r2 r cs8.( e16)~
  | % Bar 2
  % e16 r8 g r cs, r e16~ e2~      % <-- version rythmique simplifiée
  e16 r8 g r cs,16~ cs8 e8~ e2~
  | % Bar 3
  e2 d4 r8 d-. b'4(
  | % Bar 4
  a) g8( a b4) r2

  | % Bar 5
  r2 r cs,8.( e16)~
  | % Bar 6
  % e16 r8 g r cs, r e16~ e2~      % <-- version rythmique simplifiée
  e16 r8 g r cs,16~ cs8 e8~ e2~
  | % Bar 7
  e1~ e4
  | % Bar 8
  R1*5/4

  | % Bar 9
  r2 r cs8.( e16)~
  | % Bar 10
  % e16 r8 g r cs, r e16~ e2~      % <-- version rythmique simplifiée
  e16 r8 g r cs,16~ cs8 e8~ e2~
  | % Bar 11
  e2 d4 r8 d-. b'4(
  | % Bar 12
  a) g8( a b4) a8( g) e4

  | % Bar 13
  r2 r cs8.( e16)~
  | % Bar 14
  % e16 r8 g r cs, r e16~ e2~      % <-- version rythmique simplifiée
  e16 r8 g r cs,16~ cs8 e8~ e2~
  | % Bar 15
  e1~ e4
  | % Bar 16
  R1*5/4
}

"Gimmick 1 - Silence" = R1*5/4 * 16
"Gimmick 1 - Silence caché" = s1*5/4 * 16
