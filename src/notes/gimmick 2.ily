"Gimmick 2 - Voix 1 - a" = \relative c' {
  % r1*5/4 * 2
  r8 c16 r e( fs) r8 fs16( g) r8 g16( a) r8 a16( b) r8
  r8 c,16 r e( fs) r8 fs16( g) r8 g16( a) r8 a16( b) r8
}

"Gimmick 2 - Voix 1 - b" = \relative c' {
  % r1*5/4 * 2
  r8 b16 r e( fs) r8 fs16( g) r8 g16( a) r8 a16( b) r8
  r8 a,16 r e'( fs) r8 fs16( g) r8 g16( fs) r8 fs16( e) r8
}

"Gimmick 2 - Voix 2 - a" = \relative c' {
  r8 g16 r c( d) r8 d16( e) r8 e16( fs) r8 fs16( g) r8
  r8 g,16 r c( d) r8 d16( e) r8 e16( fs) r8 fs16( g) r8
}

"Gimmick 2 - Voix 2 - b" = \relative c' {
  r8 g16 r c( d) r8 d16( e) r8 e16( fs) r8 fs16( g) r8
  r8 fs,16 r c'( d) r8 d16( e) r8 e16( d) r8 d16( c) r8
}

% OLD VOIX 2:

% "Gimmick 2 - Voix 3 - a" = \relative c' {
%   r8 e16 r g( a) r8 a16( b) r8 b16( c) r8 c16( d) r8
%   r8 e,16 r g( a) r8 a16( b) r8 b16( c) r8 c16( d) r8
% }

% "Gimmick 2 - Voix 3 - b" = \relative c' {
%   r8 e16 r g( a) r8 a16( b) r8 b16( c) r8 c16( d) r8
%   r8 d,16 r g( a) r8 a16( b) r8 b16( a) r8 a16( g) r8
% }

"Gimmick 2" = \relative c' {
  \"Gimmick 2 - Voix 1 - a"
  \"Gimmick 2 - Voix 1 - b"
  \"Gimmick 2 - Voix 1 - a"
}

"Gimmick 2 - Silence" = R1*5/4 * 6
"Gimmick 2 - Silence caché" = s1*5/4 * 6

"Gimmick 2b - Voix 1" = \relative c' {
  \"Gimmick 2 - Voix 1 - a"
  \"Gimmick 2 - Voix 1 - b"
  \"Gimmick 2 - Voix 1 - a"
  \"Gimmick 2 - Voix 1 - b"
  \"Gimmick 2 - Voix 1 - a"
  \"Gimmick 2 - Voix 1 - b"
  \"Gimmick 2 - Voix 1 - a"
}

"Gimmick 2b - Voix 2" = \relative c' {
  R1*5/4 * 8
  \"Gimmick 2 - Voix 2 - a"
  \"Gimmick 2 - Voix 2 - b"
  \"Gimmick 2 - Voix 2 - a"
}

"Gimmick 2b - Silence" = R1*5/4 * 14
"Gimmick 2b - Silence caché" = s1*5/4 * 14
