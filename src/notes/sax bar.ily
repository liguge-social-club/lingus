\include "./basse.ily"

"Sax bar - Refrain B' __ c" = \relative c {
  c16( g' a c) r g( c,) r r2.
}

"Sax bar - Refrain B' __ d" = \relative c {
  d16( a' c d) r a( d,) r r2.
}

"Sax bar - Refrain B' __ a" = \relative c {
  a16( e' g a) r e( a,) r r2.
}

"Sax bar - Refrain B' __ e" = \relative c {
  e16( b' d e) r b( e,) r r2.
}

"Sax bar - Refrain B' - sans transpo" = \relative c {
  R1*5/4
  \"Sax bar - Refrain B' __ c"
  \"Sax bar - Refrain B' __ d"
  \"Sax bar - Refrain B' __ a"
  \"Sax bar - Refrain B' __ e"
  \"Sax bar - Refrain B' __ c"
  \"Sax bar - Refrain B' __ d"
  \"Sax bar - Refrain B' __ a"
}

"Sax bar - Refrain B'" = \relative c <<
  {
    s1*5/4
    s1*5/4^\markup { \italic "(Octave au choix)" }
  }
  \"Sax bar - Refrain B' - sans transpo"
  \transpose c c' \"Sax bar - Refrain B' - sans transpo"
>>

"Sax bar - Gimmick 1b avec fin de phrase" = \relative c, {
  \"Basse - Gimmick 1 - Phrase de base"

  | e8. r16 e8( g) a4 a8( b) g4
  | r2 r a8( b)
  | c4 b8( a g a b4) g
  | e4
  \tuplet 3/2 { r8 cs'' g  }
  \tuplet 3/2 { a  b   g  }
  \tuplet 3/2 { e  cs  b  }
  \tuplet 3/2 { g  b   cs }

  \repeat unfold 2 \"Basse - Gimmick 1 - Phrase de base"
}
