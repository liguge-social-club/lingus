\include "../notes/drums.ily"

"Staff - Drums" = <<
   \new DrumStaff \"Drums - Snare"
   \new DrumStaff \"Drums - Hit hat"
   \new DrumStaff \"Drums - Shake"
   \new DrumStaff \"Drums - Toms"
   \new DrumStaff \"Drums - Kick"
   \new DrumStaff \"Drums - Crash"
   \new DrumStaff \"Drums - Ride"
>>
