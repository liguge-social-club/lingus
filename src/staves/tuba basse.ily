\include "../parts/tuba basse.ily"

%------------------------------------  NOM-------  CLEF--  MUSIQUE----------------------  VOL. MIDI------  INSTR. MIDI------
"Staff - Tuba basse" =         \staff  "Tuba bs."  bass    \"Part - Tuba basse"           \dyn_full_range  "tuba"
"Staff - Tuba basse - Midi" =  \staff  ""          bass    \"Part - Tuba basse - Midi"    \dyn_full_range  "tuba"
