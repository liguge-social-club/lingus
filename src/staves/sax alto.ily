\include "../parts/sax alto.ily"

%---------------------------------  NOM-------  CLEF--  MUSIQUE-----------------------  VOL. MIDI--  INSTR. MIDI------
"Staff - Saxophone alto" =        \staff  "Sax alt"   treble  \"Part - Saxophone alto"        \dyn_middle  "alto sax"
"Staff - Saxophone alto - Midi" = \staff  ""          treble  \"Part - Saxophone alto - Midi" \dyn_middle  "alto sax"
