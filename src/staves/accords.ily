\include "../notes/accords.ily"

%-----------------------------  NOM-------  CLEF--  MUSIQUE----------------  VOL. MIDI------  INSTR. MIDI------
"staff - Banjo"      =  \staff  "Accords"   treble  \"accords"                \dyn_middle      "banjo"
"staff - Orgue"      =  \staff  "Accords"   treble  \"accords"                \dyn_full_range  "rock organ"
