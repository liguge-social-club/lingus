\include "../parts/clarinettes.ily"

%------------------------------------  NOM----  CLEF--  MUSIQUE---------------------  VOL. MIDI--  INSTR. MIDI------
"Staff - Clarinettes" =        \staff  "Clar."  treble  \"Part - Clarinettes"         \dyn_loud    "clarinet"
"Staff - Clarinettes - Midi" = \staff  ""       treble  \"Part - Clarinettes - Midi"  \dyn_loud    "clarinet"
