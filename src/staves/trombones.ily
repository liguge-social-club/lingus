\include "../parts/trombones.ily"

%----------------------------------  NOM---  CLEF--  MUSIQUE-------------------  VOL. MIDI------  INSTR. MIDI------
"Staff - Trombones" =        \staff  "Trb."  bass    \"Part - Trombones"         \dyn_full_range  "trombone"
"Staff - Trombones - Midi" = \staff  ""      bass    \"Part - Trombones - Midi"  \dyn_full_range  "trombone"
