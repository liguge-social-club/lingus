\include "../parts/sax bar.ily"

%------------------------------------------  NOM------  CLEF--  MUSIQUE-------------------------------------------  VOL. MIDI--  INSTR. MIDI------
"Staff - Saxophone baryton" =        \staff  "Sax bar"  treble  \transpose c c'' \"Part - Saxophone baryton"        \dyn_loud    "baritone sax"
"Staff - Saxophone baryton - Midi" = \staff  ""         treble  \transpose c c \"Part - Saxophone baryton - Midi"   \dyn_loud    "baritone sax"
