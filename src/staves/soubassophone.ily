\include "../parts/tuba basse.ily"

%---------------------------------------  NOM------  CLEF--  MUSIQUE--------------------  VOL. MIDI------  INSTR. MIDI------
"Staff - Soubassophone" =         \staff  "Soubas."  treble  \"Part - Tuba basse"         \dyn_full_range  "tuba"
"Staff - Soubassophone - Midi" =  \staff  ""         bass    \"Part - Tuba basse - Midi"  \dyn_full_range  "tuba"
