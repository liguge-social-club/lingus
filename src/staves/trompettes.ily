\include "../parts/trompettes.ily"

%-----------------------------------  NOM--  CLEF--  MUSIQUE--------------------  VOL. MIDI------  INSTR. MIDI------
"Staff - Trompettes"        = \staff  "Tr."  treble  \"Part - Trompettes"         \dyn_full_range  "trumpet"
"Staff - Trompettes - Midi" = \staff  ""     treble  \"Part - Trompettes - Midi"  \dyn_full_range  "trumpet"
