\version "2.18.2"
\language "english"

\include "articulate.ly"

\include "./include/env_util.ily"
#(begin
  (define TEMPO (number-from-env "TEMPO" 120))
  (define SONG_NAME (string-from-env "SONG_NAME" "No name"))
)

staffCommon = {
  \key e \minor
  \time 5/4
  \tempo 4 = \TEMPO
}

defaultPartStaffSize = #18

\include "./include/misc_util.ily"
\include "./include/find_first.ily"
\include "./include/add_articulations.ily"
\include "./include/repeat_percent.ily"
\include "./marks.ily"
\include "./include/midi.ily"
\include "./include/add_midi_ticks.ily"
\include "./include/staves.ily"
\include "./style/global.ily"
