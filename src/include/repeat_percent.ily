repeatPercent = #(define-music-function (nbtimes mus) (integer? ly:music?)
  (if IS_CONDUCTOR
    #{
      \repeat unfold #nbtimes  { #mus }
    #}
    #{
      \set countPercentRepeats = ##t
      \repeat percent #nbtimes { #mus }
    #}
  )
)
