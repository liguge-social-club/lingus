addArticulationsToFirstNote = #(define-music-function (from to) (ly:music? ly:music?)
  (let ((f (findFirst from note-or-rest?)) (t (findFirst to note?)))
     (set! (ly:music-property t 'articulations)
       (append (ly:music-property f 'articulations)
         (ly:music-property t 'articulations)))
     to
  )
)
