% Dynamiques MIDI:
dyn_solo       = #(cons 0   1.5)
dyn_full_range = #(cons 0   1.0)
dyn_loud       = #(cons 0.1 0.8)
dyn_middle     = #(cons 0.1 0.5)
dyn_soft       = #(cons 0.1 0.2)

%% Dynamiques utiles pour écouter un instru en particulier:
% dyn_solo       = #(cons 0   1.5)
% dyn_full_range = #(cons 0   0.5)
% dyn_loud       = #(cons 0.1 0.4)
% dyn_middle     = #(cons 0.1 0.3)
% dyn_soft       = #(cons 0.1 0.2)

midiVolume = #(define-music-function (vol) (pair?) #{
  \set Staff.midiMaximumVolume = #(cdr vol)
  \set Staff.midiMinimumVolume = #(car vol)
#})
