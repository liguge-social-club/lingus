#(begin

  (define (string->boolean str) (string=? str "1"))

  (define (from-env env-varname convertor default)
    (let ((env-var (getenv env-varname)))
      (if (string? env-var)
        (convertor env-var)
        default
      )))

  (define (number-from-env env-varname default)
    (from-env env-varname string->number default))

  (define (boolean-from-env env-varname default)
    (from-env env-varname string->boolean default))

  (define (self value)
    value)

  (define (string-from-env env-varname default)
    (from-env env-varname self default))

)
