#(define (note? v)
 (and (ly:music? v) (eq? (ly:music-property v 'name) 'NoteEvent))
)

#(define (rest? v)
 (and
   (ly:music? v)
   (let ((name (ly:music-property v 'name)))
     (or
       (eq? name 'RestEvent)
       (eq? name 'SkipEvent)
     )))
)

#(define (note-or-rest? v)
 (or (note? v) (rest? v))
)

#(define (__findFirstInList events pred?)
  (if (null? events)
    #f
    (let ((event (findFirst (car events) pred?)))
      (if (ly:music? event)
        event
        (__findFirstInList (cdr events) pred?)))))

findFirst = #(define-scheme-function (music pred?) (ly:music? procedure?)
  (cond
    ((pred? music) music)
    ((not(null? (ly:music-property music 'elements)))
     (__findFirstInList (ly:music-property music 'elements) pred?))
    ((not(null? (ly:music-property music 'element)))
     (findFirst (ly:music-property music 'element) pred?))
    (else #f)
  )
)
