"__addMidiTicks_makeNote" = #(define-music-function
  (drum-type bar-time dynamic-event)
  (symbol? integer? string?)
  #{
     #(make-music
         'NoteEvent
         'articulations
         (list (make-music
                 'AbsoluteDynamicEvent
                 'text
                 dynamic-event))
         'drum-type
         drum-type
         'duration
         (__addMidiTicks_makeDurationFromTimeNotation bar-time)
     )
  #}
)

addMidiTicks = #(define-music-function
  (bar-length bar-time notes)
  (integer? integer? ly:music?)
  #{ <<
    $notes
    \new Devnull \with {
      \consists Drum_note_performer
      \consists Staff_performer
      \consists Dynamic_performer
      midiInstrument = #"woodblock"
    } \drummode {
      \repeat unfold
      $(ly:moment-main-numerator
        (ly:moment-div
         (ly:music-length notes)
         (ly:make-moment bar-length bar-time)))
      {
        #(__addMidiTicks_makeNote 'cowbell bar-time "ffff")

        \repeat unfold $(- bar-length 1) {
          #(__addMidiTicks_makeNote 'closedhihat bar-time "ff")
        }
      }
    }
  >> #}
)

#(define (__addMidiTicks_makeDurationFromTimeNotation rythmic-notation)
  (ly:make-duration
    (case rythmic-notation
      ((16) 4)
      ((8)  3)
      ((4)  2)
      ((2)  1)
      ((1)  0)
    )
  )
)
