staff = #(define-music-function
  (name    clef    music     vol   midiName)
  (string? string? ly:music? pair? string?)
  #{
    \new Staff \with {
      instrumentName = #name
      shortInstrumentName = #name
      midiInstrument = #midiName
      \consists "Merge_rests_engraver"
    } {
      \clef #clef
      \staffCommon
      \midiVolume #vol
      \compressMMRests
      \new Voice <<
         \"rehearsal marks"   %"
         #music
      >>
    }
  #})

combine = #(define-music-function (music1 music2) (ly:music? ly:music?)
  #{
    \partCombine
      \new Voice {
        \voiceOne
        #music1
      }
      \new Voice {
        \voiceTwo
        #music2
      }
  #})

staffTwo = #(define-music-function
  (name    clef    music1    music2    vol   midiName)
  (string? string? ly:music? ly:music? pair? string?)
  #{
    \staff #name #clef \combine #music1 #music2 #vol #midiName
  #})
