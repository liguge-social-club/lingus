#(set-global-staff-size defaultPartStaffSize)

\layout {
  \context {
    \Staff
    \remove "Instrument_name_engraver"
  }
}

\paper {
  markup-system-spacing = #'(
    (basic-distance . 18)
    (minimum-distance . 6)
    (padding . 1)
    (stretchability . 12)
  )
  system-system-spacing = #'(
    (basic-distance . 11)
    (minimum-distance . 6)
    (padding . 1)
    (stretchability . 12)
  )
}
