\include "../notes/intro.ily"
\include "../notes/basse.ily"

"Part - Tuba basse" = {
  | % Bar 1
  \"Intro - Silence"
  | % Bar 5
  \"Basse - Gimmick 1"
  | % Bar 21
  \"Basse - Gimmick 2"
  | % Bar 27
  \"Basse - Refrain"
  | % Bar 35
  \"Basse - Gimmick 1"
  \"Basse - Gimmick 2b"
  \"Basse - Refrain"
  \"Basse - Refrain B'"
}
"Part - Tuba basse - Midi" = \"Part - Tuba basse"
