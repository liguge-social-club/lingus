\include "../notes/intro.ily"
\include "../notes/accords rythmiques.ily"
\include "../notes/accords.ily"

"Part - Trombones" = <<
  {
    | % Bar 1
    \addArticulationsToFirstNote { r\p } \"Accords rythmiques - Intro - Voix 1"
    | % Bar 5
    \"Accords rythmiques - Gimmick 1 - Voix 1"
    | % Bar 21
    \"Accords rythmiques - Gimmick 2A - Voix 1"
    \"Accords - Refrain - Voix 1"
    \"Accords rythmiques - Gimmick 1 - Voix 1"
    \"Accords rythmiques - Gimmick 2B - Voix 1"
    \"Accords - Refrain - Voix 1"
    \"Accords - Refrain B' - Voix 2"
  }
  {
    | % Bar 1
    \"Accords rythmiques - Intro - Voix 3"
    | % Bar 5
    \"Accords rythmiques - Gimmick 1 - Voix 3"
    | % Bar 21
    \"Accords rythmiques - Gimmick 2A - Voix 3"
    \"Accords - Refrain - Voix 3"
    \"Accords rythmiques - Gimmick 1 - Voix 3"
    \"Accords rythmiques - Gimmick 2B - Voix 3"
    \"Accords - Refrain - Voix 3"
    % \"Refrain B2 - Patate au début"
    \"Accords - Refrain B' - Voix 3"
  }
>>

"Part - Trombones - Midi" = \"Part - Trombones"
