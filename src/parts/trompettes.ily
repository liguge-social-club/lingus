\include "../notes/intro.ily"
\include "../notes/gimmick 1.ily"
\include "../notes/gimmick 2.ily"
\include "../notes/refrain.ily"

"Part - Trompettes" = {
  | % Bar 1
  \"Intro - Silence"
  | % Bar 5
  \"Gimmick 1"
  | % Bar 21
  \"Gimmick 2"
  | % Bar 27
  \"Refrain - Fin Gimmick 2 + Silence"
  | % Bar 35
  \"Gimmick 1"
  \"Gimmick 2b - Voix 1"
  \"Refrain B - Trompettes"
  \"Refrain B2 simplifié"
}

"Part - Trompettes - Midi" = \"Part - Trompettes"
