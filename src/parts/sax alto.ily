\include "../notes/intro.ily"
\include "../notes/gimmick 1.ily"
\include "../notes/gimmick 2.ily"
\include "../notes/refrain.ily"
\include "../notes/accords.ily"

"Part - Saxophone alto" = {
  | % Bar 1
  \"Intro - Silence"
  | % Bar 5
  \"Gimmick 1 - Silence"
  | % Bar 21
  \"Gimmick 2 - Silence"
  | % Bar 27
  \addArticulationsToFirstNote { r4\f }
  \"Refrain"
  | % Bar 35
  \"Gimmick 1 - Silence"
  \"Gimmick 2b - Voix 2"
  \"Refrain"
  % \transpose c c' \"Refrain B2 - Patate au début"
  \transpose c c \"Accords - Refrain B' - Voix 1"
}
"Part - Saxophone alto - Midi" = \"Part - Saxophone alto"
