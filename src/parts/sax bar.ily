\include "../notes/intro.ily"
\include "../notes/refrain.ily"
\include "../notes/basse.ily"
\include "../notes/sax bar.ily"
\include "../include/add_articulations.ily"

"Part - Saxophone baryton" = {
  | % Bar 1
  \"Intro - Silence"
  | % Bar 5
  \"Basse - Gimmick 1"
  | % Bar 21
  \transpose c c' \"Basse - Gimmick 2"
  | % Bar 27
  \transpose c' c \"Refrain"
  | % Bar 35
  \"Sax bar - Gimmick 1b avec fin de phrase"
  \transpose c c' \"Basse - Gimmick 2b"
  \transpose c' c \"Refrain"
  \addArticulationsToFirstNote { r4\ff }
  \transpose c' c \"Sax bar - Refrain B'"
}
"Part - Saxophone baryton - Midi" = \"Part - Saxophone baryton"
