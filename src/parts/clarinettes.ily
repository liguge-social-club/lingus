\include "../notes/intro.ily"
\include "../notes/gimmick 1.ily"
\include "../notes/gimmick 2.ily"
\include "../notes/refrain.ily"
\include "../notes/accords rythmiques.ily"

"Part - Clarinettes" = <<
  {
    \"Intro - Silence"
    \"Gimmick 1 - Silence caché"
    \"Gimmick 2 - Silence caché"
    \addArticulationsToFirstNote { r\f }
    \"Refrain"
    \"Gimmick 1 - Silence"
    \addArticulationsToFirstNote { r\mf }
    \"Gimmick 2b - Voix 2"
    \"Refrain"
  }
  \transpose c c' {
    | % Bar 1
    \addArticulationsToFirstNote { r\mf }
    \"Accords rythmiques - Intro - Voix 2"
    | % Bar 5
    \"Accords rythmiques - Gimmick 1 - Voix 2"
    | % Bar 21
    \"Accords rythmiques - Gimmick 2A - Voix 2"
    \break
    \"Refrain - Silence caché"
    \"Gimmick 1 - Silence caché"
    \"Gimmick 2b - Silence caché"
    \"Refrain - Silence caché"
    \"Refrain B2 - Patate au début"
  }
>>

"Part - Clarinettes - Midi" = \"Part - Clarinettes"
